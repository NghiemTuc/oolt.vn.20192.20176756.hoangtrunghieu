package hust.soict.hedspi.gui.awt;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;

public class WindowEvent extends Frame implements ActionListener, WindowListener{

	@Override
	public void windowOpened(java.awt.event.WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(java.awt.event.WindowEvent e) {
		System.exit(0);
		
	}

	@Override
	public void windowClosed(java.awt.event.WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(java.awt.event.WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(java.awt.event.WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(java.awt.event.WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(java.awt.event.WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		++count;
	    tfCount.setText(count + "");
		
	}
	
	private TextField tfCount;  
	private Button btnCount;    
	private int count = 0;      
	
	public WindowEvent() {
	      setLayout(new FlowLayout()); 
	      add(new Label("Counter"));  
	      tfCount = new TextField("0", 10); 
	      tfCount.setEditable(false);       
	      add(tfCount);                     
	      btnCount = new Button("Count");  
	      add(btnCount);                   
	
	      btnCount.addActionListener(this);

	      addWindowListener(this);
	 
	      setTitle("WindowEvent"); 
	      setSize(250, 100);           
	      setVisible(true);             
	}
	
	public static void main(String[] args) {
	      new WindowEvent(); 
	   }
	
}
