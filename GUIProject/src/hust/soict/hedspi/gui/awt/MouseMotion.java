package hust.soict.hedspi.gui.awt;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class MouseMotion extends Frame implements MouseListener, MouseMotionListener{

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		tfMousePositionX.setText(e.getX() + "");
	    tfMousePositionY.setText(e.getY() + "");
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		tfMouseClickX.setText(e.getX() + "");
	    tfMouseClickY.setText(e.getY() + "");
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	private TextField tfMouseClickX;
	private TextField tfMouseClickY;
	private TextField tfMousePositionX;
	private TextField tfMousePositionY;
	
	public MouseMotion() {
		setLayout(new FlowLayout()); 

		add(new Label("X-Click: "));
		tfMouseClickX = new TextField(10);
		tfMouseClickX.setEditable(false);
		add(tfMouseClickX);
		add(new Label("Y-Click: "));
		tfMouseClickY = new TextField(13);
		tfMouseClickY.setEditable(false);
		add(tfMouseClickY);

		add(new Label("X-Position: "));
		tfMousePositionX = new TextField(11);
		tfMousePositionX.setEditable(false);
		add(tfMousePositionX);
		add(new Label("Y-Position: "));
		tfMousePositionY = new TextField(11);
		tfMousePositionY.setEditable(false);
		add(tfMousePositionY);

		addMouseListener(this);
		addMouseMotionListener(this);
		
		setTitle("MouseMotion"); 
		setSize(400, 120);            
		setVisible(true);            
	}


	public static void main(String[] args) {
		new MouseMotion();  
	}
}
