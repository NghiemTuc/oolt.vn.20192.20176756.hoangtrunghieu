package hust.soict.hedspi.gui.awt;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AWTAccumulator extends Frame implements ActionListener{

	@Override
	public void actionPerformed(ActionEvent e) {
		int number = Integer.parseInt(tfInTextField.getText());
		sum += number;
		tfInTextField.setText("");
		tfOutTextField.setText(sum+"");
		
	}
	private Label lbInLabel;
	private Label lbOutLabel;
	private TextField tfInTextField;
	private TextField tfOutTextField;
	private int sum = 0;
	
	public AWTAccumulator() {
		setLayout(new FlowLayout());
		
		lbInLabel = new Label("Enter an integer: ");
		add(lbInLabel);
		
		tfInTextField = new TextField(10);
		add(tfInTextField);
		tfInTextField.addActionListener(this);
		
		lbOutLabel = new Label("The Accumulated sum is: ");
		add(lbOutLabel);
		
		tfOutTextField = new TextField(10);
		tfOutTextField.setEditable(false);
		add(tfOutTextField);
		
		setTitle("AWT Accmulator");
		setSize(350,120);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		AWTAccumulator appAccumulator = new AWTAccumulator();
		
	}
	//Tao confix
}
