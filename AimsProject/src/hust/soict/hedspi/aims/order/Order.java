package hust.soict.hedspi.aims.order;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JOptionPane;

import hust.soict.hedspi.aims.exception.AddMediaException;
import hust.soict.hedspi.aims.exception.CreateOrderException;
import hust.soict.hedspi.aims.exception.GetLuckyItem;
import hust.soict.hedspi.aims.exception.RemoveException;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.media.book.Book;
import hust.soict.hedspi.aims.media.disc.CompactDisc;
import hust.soict.hedspi.aims.media.disc.DigitalVideoDisc;
import hust.soict.hedspi.aims.utils.MyDate;

public class Order{		
		
		public static final int MAX_NUMBERS_ORDERED = 10;
		public static final int MAX_LIMITTED_ORDERED = 5;
		public static final int ITEM_MIN = 3;
		public static final int TOTAL_COST_MIN = 400;
		public static final float SALE = (float) 0.1;
		private MyDate myDate;
//		private Media luckyDisc = new Media();
		private List<Media> itemsOrdered = new ArrayList<Media>();
		
		private static int nbOrders = 0;
		
		public MyDate getMyDate() {
			return myDate;
		}
		public String getMydate() {
			return myDate.getDay() + "/" + myDate.getMonth() + "/" + myDate.getYear();
		}
		public void setMyDate(MyDate myDate) {
			this.myDate = myDate;
		}
		
		public List<Media> getitemsOrdered(){
			return this.itemsOrdered;
		}
		
		private Order() {
			
		}

		public static Order createOrder() throws CreateOrderException{
			if (nbOrders < MAX_LIMITTED_ORDERED) {
				nbOrders++;
				Order objOrder = new Order();
				objOrder.myDate = new MyDate();
				JOptionPane.showMessageDialog(null, "Create Order thanh cong");
				return objOrder;
			}else {
				throw new CreateOrderException("Ban da create qua so luong cho phep");
			}
		}
		
		public void printnbOrder() {
			System.out.println(nbOrders);
		}
//		public boolean check(Media media) {
//			for(int i = 0 ;i<this.itemsOrdered.size();i++)
//				if(this.itemsOrdered.get(i).equals(media))
//					return true;
//			return false;
//		}
		public void addMedia(Media media) throws AddMediaException{
			if (itemsOrdered.size() == MAX_NUMBERS_ORDERED) {
				throw new AddMediaException("Order da day");
			}
			for(int i = 0 ;i<this.itemsOrdered.size();i++)
				if(this.itemsOrdered.get(i).equals(media)) {
					throw new AddMediaException("Da ton tai san pham co id = "+ media.getId());
				}
			if(media instanceof CompactDisc) {
				CompactDisc compactDisc = (CompactDisc) media;
				if(compactDisc.numberTracks() == 0)
					throw new AddMediaException("Khong the add CD do CD khong co 1 tracks nao");
				if(!compactDisc.isOk())
					throw new AddMediaException("Khong the add CD do CD co 1 tracks nao do khong the phat");
			}
			else if(media instanceof DigitalVideoDisc) {
				DigitalVideoDisc dvd = (DigitalVideoDisc) media;
				if(dvd.getLength() <= 0)
					throw new AddMediaException("Khong the add DVD do DVD phai co do dai lon hon 0");
			}
			itemsOrdered.add(media);
			String message = "Them ";
			if(media instanceof DigitalVideoDisc) {
				message += "DVD: ";
			}
			else if (media instanceof Book){
				message += "Book: ";
			}
			else {
				message += "CD: ";
			}
			message += media.getTitle() + " thanh cong voi id = " + media.getId();
			JOptionPane.showMessageDialog(null, message);
		}
		public void removeMedia(Media media) throws RemoveException{
			for(int i = 0 ;i<this.itemsOrdered.size();i++)
				if(this.itemsOrdered.get(i).equals(media)) {
					itemsOrdered.remove(media);
					return;
				}
			throw new RemoveException("Khong ton tai san pham co id = "+ media.getId());
		}
		public void removeMedia(String id) throws RemoveException {
			for(int i = 0 ; i < itemsOrdered.size();i++) {
				if(itemsOrdered.get(i).getId().equals(id)) {
					Media media = itemsOrdered.get(i);
					System.out.println("Xoa "+ media.getTitle() + " thanh cong voi id = " + media.getId());
					itemsOrdered.remove(media);
					return;
				}					
			}
			throw new RemoveException("Khong ton tai media co Id = " + id);
		}
		
		public boolean isId(String id) {
			for(Media media: itemsOrdered) {
				if(media.getId().equals(id))
					return true;
			}
			return false;
		}
		
		
		public boolean isInitemsOrdered(Media media){
			for(Media media2: itemsOrdered) {
				if(media2.equals(media))
					return true;
			}
			return false;
		}
		
		
		public float totalCost() {
			float sum = 0;
			for(int i = 0; i < itemsOrdered.size() ; i++) 
				sum += itemsOrdered.get(i).getCost();			
			return sum;			
		}
		
		public void printOrder(int oppdate) {
			if (!isEmpty()) {
//				this.luckyDisc = this.getALuckyItem();
				Media lucky = new DigitalVideoDisc("0", "0", "0", 0, 0, "0");
				try {
					lucky = this.getALuckyItem();
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
				System.out.println("**************************Order**************************");
				System.out.print("Date: ");
				this.myDate.print(oppdate);	// Them opption ngay thang o day
				System.out.println("Ordered items:");
				System.out.println("STT) Id. Type - Title - Category - Cost");
				for(int i = 0 ; i < itemsOrdered.size(); i++) {
					System.out.print((i+1)+") ");
					System.out.print(itemsOrdered.get(i).getId()+". ");
					if (itemsOrdered.get(i) instanceof Book) {
						System.out.print("Book- ");
					}
					else if (itemsOrdered.get(i) instanceof DigitalVideoDisc) {
						System.out.print("DVD- ");
					}
					else {
						System.out.print("CD- ");
					}
					System.out.print(itemsOrdered.get(i).getTitle()+ " - "+ itemsOrdered.get(i).getCategory()+": ");
					if(lucky.equals(itemsOrdered.get(i))) {
						System.out.println("0.0 $");
					}
					else {
						System.out.println(itemsOrdered.get(i).getCost() + " $");
					}
				}
				System.out.println("Total cost: " + (this.totalCost() - lucky.getCost()) + " $");
				System.out.println("*********************************************************");
			}
			else {
				System.out.println("Order empty");
			}
		}
		public boolean isEmpty() {
			if(this.itemsOrdered.size()>0) {
				return false;
			}
			return true;
		}
		public void printOrder() {
			if (!isEmpty()) {
//				this.luckyDisc = this.getALuckyItem();
				Media lucky = new DigitalVideoDisc("0", "0", "0", 0, 0, "0");
				try {
					lucky = this.getALuckyItem();
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
				
				System.out.println("**************************Order**************************");
				System.out.print("Date: ");
				this.myDate.print();	// Them opption ngay thang o day
				System.out.println("Ordered items:");
				System.out.println("STT) Id. Type - Title - Category - Cost");
				for(int i = 0 ; i < itemsOrdered.size(); i++) {
					System.out.print((i+1)+") ");
					System.out.print(itemsOrdered.get(i).getId()+". ");
					if (itemsOrdered.get(i) instanceof Book) {
						System.out.print("Book- ");
					}
					else if (itemsOrdered.get(i) instanceof DigitalVideoDisc) {
						System.out.print("DVD- ");
					}
					else {
						System.out.print("CD- ");
					}
					System.out.print(itemsOrdered.get(i).getTitle()+ " - "+ itemsOrdered.get(i).getCategory()+": ");
					if(lucky.equals(itemsOrdered.get(i))) {
						System.out.println("0.0 $");
					}
					else {
						System.out.println(itemsOrdered.get(i).getCost() + " $");
					}
				}
				System.out.println("Total cost: " + (this.totalCost() - lucky.getCost()) + " $");
				System.out.println("*********************************************************");
			}
			else {
				System.out.println("Order empty");
			}
		}
		public void sortOrder() {
			Collections.sort((ArrayList<Media>)this.itemsOrdered);
			System.out.println("Order sorted ");
		}
		public Media getALuckyItem() throws GetLuckyItem{
			if(itemsOrdered.size() < ITEM_MIN && this.totalCost() < TOTAL_COST_MIN) {
				throw new GetLuckyItem("Ban can order them it nhat " + (ITEM_MIN-itemsOrdered.size()) + " san pham.\n" + 
			"Tong gia tri order them phai toi thieu " + (TOTAL_COST_MIN - this.totalCost()) + " $.");
			}
			if(itemsOrdered.size() < ITEM_MIN ) {
				throw new GetLuckyItem("Ban can order them it nhat " + (ITEM_MIN-itemsOrdered.size()) + " san pham.");
			}
			if(this.totalCost() < TOTAL_COST_MIN) {
				throw new GetLuckyItem("Ban can order them it nhat " + (TOTAL_COST_MIN - this.totalCost()) + " $.");
			}
			ArrayList<Media> luckyArrayList = new ArrayList<Media>();
			for(Media media: itemsOrdered) {
				if(media.getCost() <= TOTAL_COST_MIN * SALE) 
					luckyArrayList.add(media);
			}
			if(luckyArrayList.size() == 0) {
				throw new GetLuckyItem("Ban da du dieu kien de nhan qua."
						+ "\nNhung gia tri phan qua toi da ma ban co the nhan duoc la: " 
						+ (TOTAL_COST_MIN * SALE) + " $.");
			}
			double phantu =  Math.random()*luckyArrayList.size();	
			return luckyArrayList.get((int) phantu) ;		
		}

}

