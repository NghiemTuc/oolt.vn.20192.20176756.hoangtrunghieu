package hust.soict.hedspi.aims.media.disc;

import hust.soict.hedspi.aims.exception.PlayerException;

public interface Playable {
	public void play()throws PlayerException;
}
