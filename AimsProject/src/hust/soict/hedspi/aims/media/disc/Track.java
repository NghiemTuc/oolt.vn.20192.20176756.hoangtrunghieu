package hust.soict.hedspi.aims.media.disc;

import javax.swing.JOptionPane;

import hust.soict.hedspi.aims.exception.PlayerException;

public class Track implements Playable{
	private String title;
	private int length;
	public String getTitle() {
		return title;
	}
//	public void setTitle(String title) {
//		this.title = title;
//	}
	public boolean equals(Object obj) {
		Track ob = (Track) obj;
		if (ob.getTitle().equals(this.getTitle()) && ob.getLength() == this.getLength())
			return true;
		return false;
	}
	public int getLength() {
		return length;
	}
//	public void setLength(int length) {
//		this.length = length;
//	}
	public Track(String title, int length) {
		super();
		this.title = title;
		this.length = length;
	}
	public void play() throws PlayerException{
		if(this.getLength() <= 0 ) {
			throw new PlayerException("Can't play track ");
		}
		else {
			String message = "Playing Track: " + this.getTitle() + "\n";
			message += "Track length: "+ this.getLength() + "\n";
			JOptionPane.showMessageDialog(null, message);
		}
	}
}
