package hust.soict.hedspi.aims.gui;


import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.TextLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.media.book.Book;
import hust.soict.hedspi.aims.media.disc.CompactDisc;
import hust.soict.hedspi.aims.media.disc.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.disc.Track;
import hust.soict.hedspi.aims.order.Order;

public class HomeFrame extends GUIFrame{
	JButton createButton = new JButton("Create new Order");;
	JButton addButton = new JButton("Add item to the order");
	JButton delButton = new JButton("Delete item by Id");
	JButton displayButton =  new JButton("Display the items of list order ");
	
	private static Order anOrder;
	
	public HomeFrame() {
		super();
//		addExitButton();
		setTitle("Order System");
		createButton.setSize(300, 40);
		createButton.setLocation(150,20);
		createButton.setFocusPainted(false);
		createButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {				
				try {
					anOrder = Order.createOrder();
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(null, e2.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		add(createButton);
		
		addButton.setSize(300, 40);
		addButton.setLocation(150,100);
		addButton.setFocusPainted(false);
		addButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (anOrder != null) {
					GUIDialog selectDialog = new GUIDialog(null);	
					selectDialog.setTitle("Select Media");
					JButton bookButton = new JButton("Book");
					bookButton.setSize(200,40);
					bookButton.setLocation(200,40);
					bookButton.setFocusPainted(false);
					bookButton.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							GUIDialog bookDialog = new GUIDialog(null,"Book");	
							JTextField authorField = new JTextField();
							JLabel authorJLabel = new JLabel("Author");
							authorJLabel.setSize(200,40);
							authorJLabel.setLocation(100,180);
							bookDialog.add(authorJLabel);
							
							authorField.setSize(200, 30);
							authorField.setLocation(300,180);
							bookDialog.add(authorField);
							
							JLabel noteJLabel = new JLabel("* Cac author cach nhau boi dau phay ' , '");
							noteJLabel.setSize(300,40);
							noteJLabel.setLocation(100,220);
							bookDialog.add(noteJLabel);
							bookDialog.okJButton.addActionListener(new ActionListener() {
								
								@Override
								public void actionPerformed(ActionEvent e) {
									bookDialog.setVisible(false);
									selectDialog.setVisible(false);	
									
										String[] outauthor = authorField.getText().split(",");
										ArrayList<String> listauthor = new ArrayList<String>();
										for(String test: outauthor) {
											listauthor.add(test);
										}
										try {
											float i = bookDialog.getCost();
											Book book = new Book(bookDialog.getId(), bookDialog.getTitleField(), bookDialog.getCategory(), i, listauthor);	
											anOrder.addMedia(book);
										} catch (Exception e2) {
											JOptionPane.showMessageDialog(null, e2.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
										}
									}
							});
	
							bookDialog.setVisible(true);
						}
					});
					selectDialog.add(bookButton);
					
					JButton dvdButton = new JButton("Digital Video Disc");
					dvdButton.setSize(200,40);
					dvdButton.setLocation(200,150);
					dvdButton.setFocusPainted(false);
					dvdButton.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							GUIDialog dvdDialog = new GUIDialog(null,"Digital Video Disc");	
							JTextField textField = new JTextField();
							JLabel textJLabel = new JLabel("Length");
							textJLabel.setSize(200,40);
							textJLabel.setLocation(100, 180);
							dvdDialog.add(textJLabel);
							textField.setSize(200,30);
							textField.setLocation(300,180);
							dvdDialog.add(textField);
							
							JTextField directorField = new JTextField();
							JLabel directorJLabel = new JLabel("Director");
							directorJLabel.setSize(200,40);
							directorJLabel.setLocation(100,220);
							dvdDialog.add(directorJLabel);
							
							directorField.setSize(200, 30);
							directorField.setLocation(300,220);
							dvdDialog.add(directorField);							
							
							dvdDialog.okJButton.addActionListener(new ActionListener() {
								
								@Override
								public void actionPerformed(ActionEvent e) {
									dvdDialog.setVisible(false);
									selectDialog.setVisible(false);
									int length;
									try {
										length = Integer.parseInt(textField.getText());
									} catch (Exception e2) {
										length = 0;
									}
									try {
										float i = dvdDialog.getCost();
										String director = directorField.getText();								
										DigitalVideoDisc dvd = new DigitalVideoDisc(dvdDialog.getId(), dvdDialog.getTitleField(), dvdDialog.getCategory(),i, length, director);
										int result = JOptionPane.showConfirmDialog(null,"Ban co muon phat DVD ?","Play",
												JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
										if(result == JOptionPane.YES_OPTION) {
											dvd.play();
										}
										anOrder.addMedia(dvd);
									} catch (Exception e2) {
										JOptionPane.showMessageDialog(null, e2.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
									}
								}
						
							});							
							dvdDialog.setVisible(true);	
						}
					});
					
					selectDialog.add(dvdButton);
					
					JButton cdButton = new JButton("Compact Disc");
					cdButton.setSize(200,40);
					cdButton.setLocation(200,250);
					cdButton.setFocusPainted(false);
					cdButton.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							GUIDialog cdDialog = new GUIDialog(null,"Compact Disc");
							
							JTextField textField = new JTextField();
							JLabel textJLabel = new JLabel("Artist");
							textJLabel.setSize(200,40);
							textJLabel.setLocation(100, 180);
							cdDialog.add(textJLabel);
							textField.setSize(200,30);
							textField.setLocation(300,180);
							cdDialog.add(textField);
							
							JTextField directorField = new JTextField();
							JLabel directorJLabel = new JLabel("Director");
							directorJLabel.setSize(200,40);
							directorJLabel.setLocation(100,220);
							cdDialog.add(directorJLabel);							
							directorField.setSize(200, 30);
							directorField.setLocation(300,220);
							cdDialog.add(directorField);
							
							JTextField trackField = new JTextField();
							JLabel trackJLabel = new JLabel("Track");
							trackJLabel.setSize(200,40);
							trackJLabel.setLocation(100,260);
							cdDialog.add(trackJLabel);							
							trackField.setSize(200, 30);
							trackField.setLocation(300,260);
							cdDialog.add(trackField);
							
							JLabel noteJLabel = new JLabel("* Track: <Track1>:<Length1>,<Track2>:<Lenght2>,...");
							noteJLabel.setSize(500,40);
							noteJLabel.setLocation(100,300);
							cdDialog.add(noteJLabel);
							
							
							cdDialog.okJButton.addActionListener(new ActionListener() {
								
								@Override
								public void actionPerformed(ActionEvent e) {
									cdDialog.setVisible(false);
									selectDialog.setVisible(false);	
									
									try {
										float i = cdDialog.getCost();	
										String director = directorField.getText();	
										CompactDisc cd = new CompactDisc(cdDialog.getId(), cdDialog.getTitleField(), cdDialog.getCategory(),i ,directorField.getText(), textField.getText());
										String[] outString = trackField.getText().split(",+");
										for(String out : outString) {
											String[] a = out.split(":+");
											Track track;
											try {
												track = new Track(a[0], Integer.parseInt(a[1]));
											} catch (Exception e2) {
												track = new Track(a[0], 0);
											}
											cd.addTrack(track);
										}
										int result = JOptionPane.showConfirmDialog(null,"Ban co muon phat CD ?","Play",
													JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
											if(result == JOptionPane.YES_OPTION) {
												cd.play();
											}
										anOrder.addMedia(cd);
						
									} catch (Exception e2) {
										JOptionPane.showMessageDialog(null, e2.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
									}

								}								
								});
							cdDialog.setVisible(true);
							
						}
					});
					selectDialog.add(cdButton);
					
					selectDialog.setVisible(true);
				}
				else {
					JOptionPane.showMessageDialog(null,"Ban chua create Order","Warning",JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		add(addButton);
		
		delButton.setSize(300, 40);
		delButton.setLocation(150,180);
		delButton.setFocusPainted(false);
		delButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (anOrder != null) {
					String strid = JOptionPane.showInputDialog(null,
					            "Moi ban nhap ID muon xoa: ",
					            "Delete ID",
					            JOptionPane.INFORMATION_MESSAGE);
					try {
						anOrder.removeMedia(strid);
						JOptionPane.showMessageDialog(null,"Xoa thanh cong Media co ID = "+ strid);
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(null, e2.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
					}
				}
				else {
					JOptionPane.showMessageDialog(null,"Ban chua create Order","Warning",JOptionPane.WARNING_MESSAGE);
				}
				
			}
		});
		add(delButton);
				
		displayButton.setSize(300, 40);
		displayButton.setLocation(150,260);
		displayButton.setFocusPainted(false);
		displayButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (anOrder != null) {
					if(anOrder.isEmpty()) {
						JOptionPane.showMessageDialog(null, "Order empty");
					}
					else {
						GUIDialog disDialog = new GUIDialog(null);
						Media luckyMedia;
						String messString = "";
						try {
							luckyMedia = anOrder.getALuckyItem();
						} catch (Exception e1) {
							messString += e1.getMessage();
							luckyMedia = new DigitalVideoDisc("0", "0", "0", 0, 0, "0");
							JOptionPane.showMessageDialog(null, messString);
						}
						disDialog.setTitle("List items of Order");
						String column_names[]= {"ID","Type","Title","Category","Cost($)"};
						List<Media> items = new ArrayList<Media>();
						items = anOrder.getitemsOrdered();
						DefaultTableModel model = new DefaultTableModel(null,column_names); 
						JTable table = new JTable(model);
						for(Media media: items) {
							String typeString ;
							if(media instanceof Book)
								typeString = "Book"; 
							else if (media instanceof DigitalVideoDisc) {
								typeString = "DVD"; 
							}
							else {
								typeString = "CD";
							}
							float cost = media.equals(luckyMedia)? 0:media.getCost();
							model.addRow(new Object[]{media.getId(),typeString,
									media.getTitle(),media.getCategory(),cost});							
						}
						model.addRow(new Object[] {"","","","",""});
						model.addRow(new Object[] {"","","Date",anOrder.getMydate(),""});
						model.addRow(new Object[] {"","","","Total: ",anOrder.totalCost()-luckyMedia.getCost()});
								
						
						table.setSize(500, 300);
						table.setLocation(50,30);
						disDialog.setLayout(new BorderLayout());
						disDialog.add(table.getTableHeader(), BorderLayout.PAGE_START);
						disDialog.add(table, BorderLayout.CENTER);					
						TableColumn column = null;
						for (int i = 0; i < 5; i++) {
						    column = table.getColumnModel().getColumn(i);
						    if (i == 0) {
						        column.setPreferredWidth(50); 
						    } 
						    if (i == 1) {
						        column.setPreferredWidth(50); 
						    }
						    if (i == 2) {
						        column.setPreferredWidth(200); 
						    }
						    if (i == 3) {
						        column.setPreferredWidth(150); 
						    }
						    if (i == 4) {
						        column.setPreferredWidth(50); 
						    }
						    
						}
						disDialog.setVisible(true);
						
					}
				}
				else {
					JOptionPane.showMessageDialog(null,"Ban chua create Order","Warning",JOptionPane.WARNING_MESSAGE);
				}
				
			}
		});
		add(displayButton);

		setVisible(true);
	}
}
