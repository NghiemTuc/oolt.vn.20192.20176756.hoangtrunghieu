package hust.soict.hedspi.aims.exception;

public class GetLuckyItem extends Exception {

	public GetLuckyItem() {
		// TODO Auto-generated constructor stub
	}

	public GetLuckyItem(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public GetLuckyItem(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public GetLuckyItem(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public GetLuckyItem(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
