package hust.soict.hedspi.aims.exception;

import javax.swing.JOptionPane;

public class AddMediaException extends Exception {

	public AddMediaException() {
		// TODO Auto-generated constructor stub
	}

	public AddMediaException(String message) {
		super(message);
	}

	public AddMediaException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public AddMediaException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AddMediaException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
