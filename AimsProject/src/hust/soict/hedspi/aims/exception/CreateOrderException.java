package hust.soict.hedspi.aims.exception;

import javax.swing.JOptionPane;

public class CreateOrderException extends Exception {

	public CreateOrderException() {
		// TODO Auto-generated constructor stub
	}

	public CreateOrderException(String message) {
		super(message);
	}

	public CreateOrderException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public CreateOrderException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CreateOrderException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
