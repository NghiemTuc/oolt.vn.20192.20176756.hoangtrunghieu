package hust.soict.hedspi.lab01;
import javax.swing.JOptionPane;

public class SecondDegreeEquation{

    public static void main(String[] args){

        String stra, strb, strc;
        double a,b,c;
        do
        {
            stra = JOptionPane.showInputDialog(
            null,
            "Moi ban nhap he so a: ",
            "He so a",
            JOptionPane.INFORMATION_MESSAGE);
            a = Double.parseDouble(stra);
        }while(a==0);
        strb = JOptionPane.showInputDialog(
            null,
            "Moi ban nhap he so b: ",
            "He so b",
            JOptionPane.INFORMATION_MESSAGE);
        strc = JOptionPane.showInputDialog(
                null,
                "Moi ban nhap he so c: ",
                "He so c",
                JOptionPane.INFORMATION_MESSAGE);
        
        b = Double.parseDouble(strb);
        c = Double.parseDouble(strc);

        double detha = b*b - 4*a*c;
        String strNotification = "Phuong trinh: " + stra + "x^2 + " + strb + "x + " + strc + " = 0\n";

        if (detha > 0) strNotification += "Phuong trinh co 2 nghiem phan biet la : " + (-b+Math.sqrt(detha))/(2*a) + " va " + (-b-Math.sqrt(detha))/(2*a);
        else if (detha == 0) strNotification += strNotification += "Phuong trinh co nghiem duy nhat la : " + -b/ (2*a);
        else strNotification += "Phuong trinh vo nghiem";

        JOptionPane.showMessageDialog(
            null, 
            strNotification,
            "Giai phuong trinh bac hai",
            JOptionPane.INFORMATION_MESSAGE);

        System.exit(0);
    }
}

