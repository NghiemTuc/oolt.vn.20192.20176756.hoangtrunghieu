package hust.soict.hedspi.lab01;
import javax.swing.JOptionPane;

public class LinearEquation{

    public static void main(String[] args){

        String strNum1, strNum2;

        strNum1 = JOptionPane.showInputDialog(
            null,
            "Moi ban he so a: ",
            "He so a",
            JOptionPane.INFORMATION_MESSAGE);

        strNum2 = JOptionPane.showInputDialog(
            null,
            "Moi ban he so b: ",
            "He so b",
            JOptionPane.INFORMATION_MESSAGE);

        double a = Double.parseDouble(strNum1);
        double b = Double.parseDouble(strNum2);

        String strNotification = "Phuong trinh : " + strNum1 + "x + " + strNum2 + " = 0";

        if (a == 0 && b == 0) strNotification += "\nPhuong trinh vo so nghiem";
        else if (a == 0 && b != 0) strNotification += "\nPhuong trinh vo nghiem";
        else strNotification += "\nPhuong trinh co nghiem duy nhat : " + -b/a;

        JOptionPane.showMessageDialog(
            null,
            strNotification,
            "LinearEquation",
            JOptionPane.INFORMATION_MESSAGE);

        System.exit(0);
    }
}
