package hust.soict.hedspi.lab01;
import javax.swing.JOptionPane;

public class Calculate{

    public static void main(String[] args){

        String strNum1, strNum2;

        strNum1 = JOptionPane.showInputDialog(
            null,
            "Moi ban nhap so thu nhat: ",
            "So thu nhat",
            JOptionPane.INFORMATION_MESSAGE);

        strNum2 = JOptionPane.showInputDialog(
            null,
            "Moi ban nhap so thu hai: ",
            "So thu hai",
            JOptionPane.INFORMATION_MESSAGE);

        double num1 = Double.parseDouble(strNum1);
        double num2 = Double.parseDouble(strNum2);

        String strNotification = "Hai so ban da nhap la " + strNum1 + " va " + strNum2;
        strNotification += "\n" + strNum1 +" + " + strNum2 + " = " + (num1+num2);
        strNotification += "\n" + strNum1 +" - " + strNum2 + " = " + (num1-num2);
        strNotification += "\n" + strNum1 +" * " + strNum2 + " = " + (num1*num2);
        strNotification += "\nThuong cua " + strNum1 +" / " + strNum2 + " = " + (num1/num2);
        strNotification += "\nSo du cua " + strNum1 +" / " + strNum2 + " = " + (num1%num2);


        JOptionPane.showMessageDialog(
            null,
            strNotification,
            "Calculate",
            JOptionPane.INFORMATION_MESSAGE);

        System.exit(0);
    }
}
