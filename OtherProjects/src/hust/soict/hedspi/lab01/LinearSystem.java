package hust.soict.hedspi.lab01;
import javax.swing.JOptionPane;

public class LinearSystem{

    public static void main(String[] args){

        String strNum11, strNum12, strNum21, strNum22,strNum1,strNum2;

        strNum11 = JOptionPane.showInputDialog(
            null,
            "Moi ban he so a11: ",
            "He so a11",
            JOptionPane.INFORMATION_MESSAGE);
        strNum12 = JOptionPane.showInputDialog(
            null,
            "Moi ban he so a12: ",
            "He so a12",
            JOptionPane.INFORMATION_MESSAGE);
        strNum1 = JOptionPane.showInputDialog(
            null,
            "Moi ban he so b1: ",
            "He so b1",
            JOptionPane.INFORMATION_MESSAGE);


        strNum21 = JOptionPane.showInputDialog(
            null,
            "Moi ban he so a21: ",
            "He so a21",
            JOptionPane.INFORMATION_MESSAGE);
        strNum22 = JOptionPane.showInputDialog(
            null,
            "Moi ban he so a22: ",
            "He so a22",
            JOptionPane.INFORMATION_MESSAGE);
        strNum2 = JOptionPane.showInputDialog(
            null,
            "Moi ban he so b2: ",
            "He so b2",
            JOptionPane.INFORMATION_MESSAGE);

        double a11 = Double.parseDouble(strNum11);
        double a12 = Double.parseDouble(strNum12);
        double a21 = Double.parseDouble(strNum21);
        double a22 = Double.parseDouble(strNum22);
        double b1 = Double.parseDouble(strNum1);
        double b2 = Double.parseDouble(strNum2);

        String strNotification = "He phuong trinh : \n" + strNum11 + "x + " + strNum12 + "y = " + strNum1;
        strNotification += "\n" + strNum21 + "x + " + strNum22 + "y = " + strNum2;

        double d = a11*a22 - a21*a12;
        double d1 = b1*a22 - b2*a12;
        double d2 = a11*b2 - a21*b1;

        if (d != 0) strNotification += "\nHe phuong trinh co nghiem duy nhat la : " + "(" + d1/d + ","+ d2/d +")";
        else if (d == 0 && d1 == 0 && d2 == 0) strNotification += "\nHe phuong trinh vo so nghiem";
        else strNotification += "\nHe phuong trinh vo nghiem";

        JOptionPane.showMessageDialog(
            null,
            strNotification,
            "LinearSystem",
            JOptionPane.INFORMATION_MESSAGE);

        System.exit(0);
    }
}
