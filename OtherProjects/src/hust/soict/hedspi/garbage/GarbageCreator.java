package hust.soict.hedspi.garbage;

import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

public class GarbageCreator {
	public static void main(String [] args) throws IOException {
		FileReader in = null;	 
	    try {
	    	in = new FileReader("Untitled6.txt");
	        String s = "";
	 		long start = System.currentTimeMillis();
	        int c;
	        while ((c = in.read()) != -1) {
	            s+= (char)c;         
	        }
	        System.out.println("Thoi gian: " + (System.currentTimeMillis()- start));
	    }catch (ArithmeticException e) {
            System.out.println(e);
        }finally {
            if (in != null) {
                in.close();
            }
        }
	}
}
