package hust.soict.hedspi.garbage;

import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

public class NoGarbage {
	public static void main(String [] args) throws IOException {
		FileReader in = null;	
		try {
	    	in = new FileReader("Untitled6.txt");
	        String s = "";
	 		long start = System.currentTimeMillis();
	        int c;
	        StringBuffer sb = new StringBuffer();
	        while ((c = in.read()) != -1) {
	            sb.append((char)c);        
	        }
	        s = sb.toString();
	        System.out.println("Thoi gian: " + (System.currentTimeMillis()- start));
	    }catch (ArithmeticException e) {
            System.out.println(e);
        }finally {
            if (in != null) {
                in.close();
            }
        }
	}
}
