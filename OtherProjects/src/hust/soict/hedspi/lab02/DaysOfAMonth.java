package hust.soict.hedspi.lab02;
import java.util.Scanner;

public class DaysOfAMonth{

    public static void main(String[] args){

        Scanner  sc = new Scanner(System.in);
        String strM;
        int mon;
        do{
        System.out.print("Moi ban nhap thang: ");
        strM = sc.nextLine();
        if (strM.equals("January") || strM.equals("Jan." )||strM.equals("Jan") || strM.equals( "1") ) mon = 1;
        else if( strM.equals("February")|| strM.equals("Feb.") ||strM.equals("Feb") || strM.equals( "2")) mon = 2;
        else if( strM.equals("March")|| strM.equals("Mar." )||strM.equals("Mar") || strM.equals( "3")) mon = 3;
        else if( strM.equals("April")|| strM.equals("Apr.") ||strM.equals("Apr" )|| strM.equals( "4")) mon = 4;
        else if( strM.equals("May")|| strM.equals("May." )||strM.equals("May" )|| strM.equals( "5")) mon = 5;
        else if( strM.equals("June")|| strM.equals("Jun." )||strM.equals("Jun" )|| strM.equals( "6")) mon = 6;
        else if( strM.equals("July")|| strM.equals("Jul." )||strM.equals("Jul" )|| strM.equals( "7")) mon = 7;
        else if( strM.equals("August")|| strM.equals("Aug.")||strM.equals("Aug" )|| strM.equals( "8")) mon = 8;
        else if( strM.equals("September")|| strM.equals("Sep.") ||strM.equals("Sep" )|| strM.equals( "9")) mon = 9;
        else if( strM.equals("October")|| strM.equals("Oct." )||strM.equals("Oct" )|| strM.equals( "10")) mon = 10;
        else if( strM.equals("November")|| strM.equals("Nov." )||strM.equals("Nov" )|| strM.equals( "11")) mon = 11;
        else if( strM.equals("December")|| strM.equals("Dec.")||strM.equals("Dec" )|| strM.equals(" 12")) mon =12;
        else{
            System.out.println("Moi ban nhap lai");
            mon = 0;
        }
        }while(mon==0);

        System.out.print("Moi ban nhap nam: ");
        int year = sc.nextInt();
        int days;

        if (mon == 1 || mon == 3 || mon == 5 || mon ==7 || mon == 8 || mon == 10 || mon == 12) days = 31;
        else if (mon == 2){
            if (year % 4 == 0 && year % 100 !=0 ) days = 29;
            else days = 28;
        } 
        else days = 30;

        System.out.println("So ngay cua thang " + mon + "/" + year + ": " + days + "days.");

        sc.close();
        System.exit(0);
    }
}