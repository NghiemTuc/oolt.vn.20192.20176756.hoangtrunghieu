package hust.soict.hedspi.lab02;
import java.util.Scanner;

public class Matrix{   
    
    public static void Display(double[][] mat){
        for (int i =0 ;i<mat.length;i++)
        {
            for (int j = 0 ; j<mat[i].length ; j++) 
                System.out.print(mat[i][j] + "\t");
            System.out.print("\n");
        }
    }
    public static void Copy_matrix(double[][] mat1, double[][] mat2){
        for (int i =0 ;i<mat1.length;i++)
            for (int j = 0 ; j<mat1[i].length ; j++) 
                mat2[i][j] = mat1[i][j];
    }
    public static double[][] Sum_matix(double[][] mat1, double[][] mat2){
        double[][] sum = new double[mat1.length][mat1[0].length];
        for (int i =0 ;i<mat1.length;i++)
            for (int j = 0 ; j<mat1[i].length ; j++) 
                sum[i][j] = mat1[i][j] + mat2[i][j];
        return sum;
    }
    
    public static void main(String[] args){
        
        int col,row;
        double[][] mat,mat2;
        final double[][] MAT_CONST = {{12,23,34},{233,546,45}};
        Scanner sc = new Scanner(System.in);
        do{
        System.out.println("Moi ban nhap thong tin cua ma tran (Neu ban nhap 0 se su dung mang co san) ");
        System.out.print("So hang cua ma tran: ");
        row = sc.nextInt();
        System.out.print("So cot cua ma tran: ");
        col = sc.nextInt();
        }while(col < 0 || row < 0);
        if (col == 0 || row == 0){
            mat = new double[MAT_CONST.length][MAT_CONST[0].length];
            mat2 = new double[MAT_CONST.length][MAT_CONST[0].length];
            Copy_matrix(MAT_CONST, mat);
            Copy_matrix(MAT_CONST, mat2);
        }
        else{
            mat = new double[row][col];
            System.out.println("Ma tran 1:");
            for (int i =0 ;i<row;i++)
                for (int j = 0 ; j<col ; j++){
                    System.out.print("Moi ban nhap phan tu Mat" + i + j + " : ");
                    mat[i][j] = sc.nextDouble();
                }
            mat2 = new double[row][col];
            System.out.println("Ma tran 2:");
            for (int i =0 ;i<row;i++)
                for (int j = 0 ; j<col ; j++){
                    System.out.print("Moi ban nhap phan tu Mat" + i + j + " : ");
                    mat2[i][j] = sc.nextDouble();
                }
        }
        System.out.println("Ma tran 1:");
        Display(mat);
        System.out.println("Ma tran 2:");
        Display(mat2);
        System.out.println("Tong 2 ma tran:");
        Display(Sum_matix(mat, mat2));
        
        sc.close();       

    }
}