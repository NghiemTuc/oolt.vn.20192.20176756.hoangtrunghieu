package hust.soict.hedspi.lab02;
import java.util.Scanner;

public class Array{    
    
    public static void Sort_array(double[] array){
        for (int i =0; i < array.length;i++)
            for (int j = i +1; j < array.length ; j++){
                if (array[i] > array[j]){
                    double tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
                }
            }
    }
    public static void Copy_array(double[] arr1, double[] arr2){
        for (int i =0 ;i<arr1.length;i++){
            arr2[i] = arr1[i];
        }
    }
    public static void Display(double[] arr){
        for (int i =0 ;i<arr.length;i++){
            System.out.print(arr[i] + "  ");
        }
        System.out.print("\n");
    }
    public static double Sum(double[] arr){
        double sum = 0;
        for (int i =0 ;i<arr.length;i++)
            sum += arr[i];
        return sum;            
    }
    public static void main(String[] args){
        
        int num;
        double[] arr;
        final double[] ARRAY_CONST = {1789,2035,1899,1456,2013};
        Scanner sc = new Scanner(System.in);
        do{
        System.out.println("Moi ban nhap so phan tu cua mang (Neu ban nhap 0 se su dung mang co san) ");
        System.out.print("So phan tu cua mang: ");
        num = sc.nextInt();
        }while(num < 0);
        if (num == 0) {
            arr = new double[ARRAY_CONST.length];
            Copy_array(ARRAY_CONST, arr);
        }
        else
        {
            arr = new double[num];
            for (int i =0;i<num;i++){
                System.out.print("Moi ban nhap so thu " + (i+1) + ": ");
                arr[i] = sc.nextDouble();
            }
        }
        System.out.print("Mang ban da nhap: ");
        Display(arr);
        Sort_array(arr);
        System.out.print("Mang sau khi sap xep: ");
        Display(arr);
        System.out.print("Tong cac phan tu trong mang : "+ Sum(arr)+ "\n");
        System.out.print("Trung binh cac phan tu trong mang : "+ Sum(arr)/arr.length+ "\n");
        sc.close();       

    }
}